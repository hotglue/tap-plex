"""Plex entry point."""

from __future__ import annotations

from tap_plex.tap import TapPlex

TapPlex.cli()
