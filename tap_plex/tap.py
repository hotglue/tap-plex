"""Plex tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_plex import streams
#streams.ApInvoicesStream
#streams.ArInvoicesStream
streams.EmployeesStream
streams.InventoryReceiptsStream
streams.InventoryShipmentsStream
streams.LotsStream
streams.PartsStream
streams.SupplyItemsStream
streams.JobStream
streams.PurchaseOrdersStream
streams.ReceiptsStream
streams.SalesOrdersStream
streams.AccountsStream


class TapPlex(Tap):
    """Plex tap class."""

    name = "tap-plex"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "project_ids",
            th.ArrayType(th.StringType),
            description="Project IDs to replicate",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
    ).to_dict()

    def discover_streams(self) -> list[streams.PlexStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.ApInvoicesStream(self),
            streams.ArInvoicesStream(self),
            streams.EmployeesStream(self),
            streams.InventoryReceiptsStream(self),
            streams.InventoryShipmentsStream(self),
            streams.LotsStream(self),
            streams.PartsStream(self),
            streams.SupplyItemsStream(self),
            streams.JobStream(self),
            streams.PurchaseOrdersStream(self),
            streams.ReceiptsStream(self),
            streams.SalesOrdersStream(self),
            streams.AccountsStream(self),
        ]


if __name__ == "__main__":
    TapPlex.cli()
