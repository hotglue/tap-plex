"""Stream type classes for tap-plex."""

from __future__ import annotations

from singer_sdk import metrics
from singer_sdk import typing as th  # JSON Schema typing helpers
from typing import Any, Dict, Iterable, List, Optional, Union
from tap_plex.client import PlexStream
from dateutil.parser import parse
from datetime import datetime, timedelta

class DatePaginationStream(PlexStream):
    #Fetch data on weekly basis
    days_delta = 7 # API doesn't accept range bigger than 8 days for this tenant.
    start_date = None
    param_range = {}
    pagination_start_key = None
    pagination_end_key = None

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        
        # Extract start_date
        if isinstance(self.start_date,str):
            start_date = parse(self.start_date)
        #Add one day to previous end_date
        if start_date:
            start_date = start_date + timedelta(days=1)

        today = datetime.today()
        next_token = start_date.replace(tzinfo=None)

        # Disable pagination if the next token's date is in the future
        if (today - next_token).days < 0:
            return None
        # Return the next token and the current skip value
        return next_token
    def get_url_params(
        self,
        context: dict | None,  # noqa: ARG002
        next_page_token: Any | None,  # noqa: ANN401
    ) -> dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization.

        Args:
            context: The stream context.
            next_page_token: The next page index or value.

        Returns:
            A dictionary of URL query parameters.
        """
        params: dict = {}
        token_date = None
        if next_page_token:
            token_date = next_page_token
        #For some reason singer-sdk function get_starting_timestamp is not picking up start_date automatically 
        start_date = token_date or self.get_starting_timestamp(context) or self.config.get("start_date") or datetime(2000, 1, 1)
        if isinstance(start_date,str):
            start_date = parse(start_date)
        end_date = start_date + timedelta(days=self.days_delta)
        params[self.pagination_start_key] = start_date.strftime("%Y-%m-%dT%H:%MZ")
        params[self.pagination_end_key] = end_date.strftime("%Y-%m-%dT23:59Z")
        self.param_range = {"start_date":params[self.pagination_start_key],"end_date":params[self.pagination_end_key]}
        self.start_date = params[self.pagination_end_key]
        return params
    
    def request_records(self, context: dict | None) -> t.Iterable[dict]:
        """
            Overriding this function because we want tap to keep paginating through the dates irrespective if there data or not. 
        """
        paginator = self.get_new_paginator()
        decorated_request = self.request_decorator(self._request)
        pages = 0

        with metrics.http_request_counter(self.name, self.path) as request_counter:
            request_counter.context = context

            while not paginator.finished:
                prepared_request = self.prepare_request(
                    context,
                    next_page_token=paginator.current_value,
                )
                resp = decorated_request(prepared_request, context)
                request_counter.increment()
                self.update_sync_costs(prepared_request, resp, context)
                records = iter(self.parse_response(resp))
                try:
                    first_record = next(records)
                except StopIteration:
                    first_record = None
                    self.logger.info(
                        f"Stream: {self.name} No records found for dates {self.param_range} "
                    )
                    #break # This is the override part we don't break the loop.
                if first_record:    
                    yield first_record
                if records:    
                    yield from records
                pages += 1

                paginator.advance(resp)

    def post_process(
        self,
        row: dict,
        context: dict | None = None,  # noqa: ARG002
    ) -> dict | None:
        if self.replication_key in row and "createdDate" in row:
            if not row[self.replication_key]:
                row[self.replication_key] = row['createdDate']
        return row            

class InvoicesStream(DatePaginationStream):
    # Possible alternatives to the two below are createdDateBegin and createdDateEnd
    pagination_start_key = "transactionDateBegin"
    pagination_end_key = "transactionDateEnd"


class ApInvoicesStream(InvoicesStream):

    name = "ap_invoices"
    path = "/accounting/v1/ap-invoices"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType, description="The invoice's system ID"),
        th.Property("invoiceNumber", th.StringType),
        th.Property("supplierId", th.UUIDType),
        th.Property("supplierCode", th.StringType),
        th.Property("supplierAddressId", th.UUIDType),
        th.Property("supplierAddressCode", th.StringType),
        th.Property("periodDisplay", th.StringType),
        th.Property("transactionDate", th.DateTimeType),
        th.Property("buildingCode", th.StringType, nullable=True),
        th.Property("currencyCode", th.StringType),
        th.Property("exchangeRate", th.NumberType),
        th.Property("department", th.NumberType, nullable=True),
        th.Property("terms", th.StringType),
        th.Property("dueDate", th.DateTimeType),
        th.Property("discount", th.NumberType),
        th.Property("internalNote", th.StringType),
        th.Property("voucherNumber", th.StringType),
        th.Property("status", th.StringType),
        th.Property("governmentIssuedNumber", th.StringType),
        th.Property("type", th.StringType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("modifiedById", th.UUIDType),
        th.Property("modifiedDate", th.DateTimeType),
        th.Property("booked", th.BooleanType),
        th.Property("taxableDate", th.DateTimeType),
        th.Property("amount", th.NumberType),
        th.Property("paid", th.BooleanType),
        th.Property("paidDate", th.DateTimeType),
        th.Property("paidPeriod", th.StringType),
        th.Property("totalControlAmount", th.NumberType, nullable=True),
        th.Property("expectedPayDate", th.DateTimeType),
        th.Property("taxId", th.StringType),
        th.Property("foreignCurrencyAmount", th.NumberType),
        th.Property("discountDueDate", th.DateTimeType, nullable=True),
        th.Property("exchangeRateDate", th.DateTimeType, nullable=True),
        th.Property("balance", th.NumberType),
        th.Property("discountableAmount", th.NumberType, nullable=True),
        th.Property("debitMemo", th.BooleanType),
        th.Property("intercompany", th.BooleanType),
        th.Property("checkNote", th.StringType),
        th.Property("shipperNo", th.StringType),
        th.Property("shipperId", th.UUIDType, nullable=True),
        th.Property("poNumber", th.StringType, nullable=True),
        th.Property("poId", th.UUIDType, nullable=True),
        th.Property("lines", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

class ArInvoicesStream(InvoicesStream):
    name = "ar_invoices"
    path = "/accounting/v1/ar-invoices"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("invoiceNumber", th.StringType),
        th.Property("customerId", th.UUIDType),
        th.Property("customerCode", th.StringType),
        th.Property("customerBillToId", th.UUIDType),
        th.Property("customerBillToCode", th.StringType),
        th.Property("customerShipToId", th.UUIDType),
        th.Property("customerShipToCode", th.StringType),
        th.Property("buildingCode", th.StringType),
        th.Property("periodDisplay", th.StringType),
        th.Property("transactionDate", th.DateTimeType),
        th.Property("currencyCode", th.StringType),
        th.Property("exchangeRate", th.NumberType),
        th.Property("terms", th.StringType),
        th.Property("dueDate", th.DateTimeType),
        th.Property("invoiceNote", th.StringType),
        th.Property("internalNote", th.StringType),
        th.Property("voucherNumber", th.StringType),
        th.Property("status", th.StringType),
        th.Property("governmentIssuedNumber", th.StringType),
        th.Property("type", th.StringType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("modifiedById", th.UUIDType),
        th.Property("modifiedDate", th.DateTimeType),
        th.Property("taxableDate", th.DateTimeType),
        th.Property("amount", th.NumberType),
        th.Property("currencyAmount", th.NumberType),
        th.Property("paid", th.BooleanType),
        th.Property("paidDate", th.DateTimeType),
        th.Property("paidPeriod", th.StringType),
        th.Property("discount", th.NumberType),
        th.Property("balance", th.NumberType),
        th.Property("currencyBalance", th.NumberType),
        th.Property("void", th.BooleanType),
        th.Property("consolidatedLinkId", th.UUIDType),
        th.Property("booked", th.BooleanType),
        th.Property("lines", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

class EmployeesStream(PlexStream):
    """
      Employees endpoint has no pagination filter support 
      https://developers.plex.com/docs/employees-api/list-employees
    """

    name = "employees"
    path = "/mdm/v1/employees"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType, description="The user's system ID"),
        th.Property("badgeNumber", th.StringType),
        th.Property("userId", th.StringType),
        th.Property("firstName", th.StringType),
        th.Property("lastName", th.StringType),
        th.Property("supervisorId", th.UUIDType),
        th.Property("status", th.StringType),
        th.Property("postition", th.StringType),
        th.Property("shift", th.StringType),
        th.Property("building", th.StringType),
        th.Property("type", th.StringType),
        th.Property("department", th.StringType),
        th.Property("email", th.StringType),
    ).to_dict()

class InventoryReceiptsStream(PlexStream):
    """
      InventoryReceiptsStream endpoint has no pagination filter support 
      https://developers.plex.com/docs/inventory-operations-management-api/list-inventory-receipts
    """

    name = "inventory_receipts"
    path = "/inventory/v1-beta1/inventory-receiving/receipts"
    schema = th.PropertiesList(
        th.Property("externalReceiptCode", th.StringType),
        th.Property("partId", th.UUIDType),
    ).to_dict()

class InventoryShipmentsStream(PlexStream):
    """
      InventoryShipmentsStream endpoint has no pagination filter support 
      https://developers.plex.com/docs/inventory-operations-management-api/list-inventory-shipments
    """

    name = "inventory_shipments"
    path = "/inventory/v1/inventory-shipping/shipments"
    schema = th.PropertiesList(
        th.Property("externalShipmentCode", th.StringType),
        th.Property("shipmentStatus", th.StringType),
    ).to_dict()

class LotsStream(PlexStream):
    """
      LotsStream endpoint filtration option based on beginManufacturedDateTime, beginBestByDateTime 
      https://developers.plex.com/docs/inventory-operations-management-api/list-lots
    """

    name = "lots"
    path = "/inventory/v1-beta1/inventory-tracking/lots"
    schema = th.PropertiesList(
        th.Property("partId", th.UUIDType),
        th.Property("partNumber", th.StringType),
        th.Property("partRevision", th.StringType),
        th.Property("lotNumber", th.StringType),
        th.Property("beginManufacturedDateTime", th.DateTimeType),
        th.Property("endManufacturedDateTime", th.DateTimeType),
        th.Property("beginBestbyDateTime", th.DateTimeType),
        th.Property("endBestbyDateTime", th.DateTimeType),
        th.Property("beginSellByDateTime", th.DateTimeType),
        th.Property("endSellByDateTime", th.DateTimeType),
        th.Property("beginUseByDateTime", th.DateTimeType),
        th.Property("endUseByDateTime", th.DateTimeType),
    ).to_dict()

class PartsStream(PlexStream):
    """
      PartsStream endpoint has no pagination filter support 
      https://developers.plex.com/docs/inventory-operations-management-api/list-parts
    """

    name = "parts"
    path = "/inventory/v1/inventory-definitions/parts"
    schema = th.PropertiesList(
        th.Property("partNumber", th.StringType),
        th.Property("partRevision", th.StringType),
        th.Property("partType", th.StringType),
        th.Property("partSource", th.StringType),
        th.Property("partStatus", th.StringType),
    ).to_dict()

class SupplyItemsStream(PlexStream):
    """
      SupplyItemsStream endpoint has no pagination filter support 
      https://developers.plex.com/docs/inventory-operations-management-api/list-supply-items
    """

    name = "supply_items"
    path = "/inventory/v1/inventory-definitions/supply-items"
    schema = th.PropertiesList(
        th.Property("supplyItemNumber", th.StringType),
        th.Property("type", th.StringType),
        th.Property("category", th.StringType),
        th.Property("group", th.StringType),
    ).to_dict()

class JobStream(DatePaginationStream):
    """Define custom stream."""

    name = "jobs"
    path = "/scheduling/v1/jobs"
    pagination_start_key = "beginEarliestStartDateTime"
    pagination_end_key = "endEarliestStartDateTime"
    schema = th.PropertiesList(
        th.Property("partId", th.UUIDType),
        th.Property("externalJobCode", th.StringType),
        th.Property("jobNumber", th.StringType),
        th.Property("jobStatuses", th.ArrayType(th.StringType)),
        th.Property("dueDateBegin", th.DateTimeType),
        th.Property("dueDateEnd", th.DateTimeType),
        th.Property("buildingId", th.UUIDType),
        th.Property("completedDateBegin", th.DateTimeType),
        th.Property("completedDateEnd", th.DateTimeType),
        th.Property("beginEarliestStartDateTime", th.DateTimeType),
        th.Property("endEarliestStartDateTime", th.DateTimeType),
        th.Property("priority", th.StringType),
    ).to_dict()

class PurchaseOrdersStream(DatePaginationStream):

    name = "purchase_orders"
    path = "/purchasing/v1/purchase-orders"
    replication_key = "modifiedDate"
    pagination_start_key = "modifiedDateBegin"
    pagination_end_key = "modifiedDateEnd"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("poNumber", th.StringType),
        th.Property("currencyCode", th.StringType),
        th.Property("status", th.StringType),
        th.Property("active", th.BooleanType),
        th.Property("receive", th.BooleanType),
        th.Property("type", th.StringType),
        th.Property("supplierId", th.UUIDType),
        th.Property("supplierAddressId", th.UUIDType),
        th.Property("issuedById", th.UUIDType),
        th.Property("poDate", th.DateTimeType),
        th.Property("terms", th.StringType),
        th.Property("freightTerms", th.StringType),
        th.Property("lineItemCount", th.IntegerType),
        th.Property("lineItemTotal", th.NumberType),
        th.Property("shipTo", th.StringType),
        th.Property("shipVia", th.StringType),
        th.Property("orderedById", th.UUIDType),
        th.Property("orderedDate", th.DateTimeType),
        th.Property("fob", th.StringType),
        th.Property("dueDate", th.DateTimeType),
        th.Property("contactId", th.UUIDType),
        th.Property("scheduledReceiptDate", th.DateTimeType),
        th.Property("approvedById", th.UUIDType),
        th.Property("approvedDate", th.DateTimeType),
        th.Property("internalNote", th.StringType),
        th.Property("note", th.StringType),
        th.Property("consignment", th.BooleanType),
        th.Property("carrierId", th.UUIDType),
        th.Property("building", th.StringType),
        th.Property("department", th.StringType),
        th.Property("routeToEmployeeId", th.UUIDType),
        th.Property("incoTerms", th.StringType),
        th.Property("weightQuantityTotal", th.NumberType),
        th.Property("blanketOrder", th.BooleanType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("modifiedById", th.UUIDType),
        th.Property("modifiedDate", th.DateTimeType),
    ).to_dict()

class ReceiptsStream(DatePaginationStream):
    """Not the best pagination keys however we should be able to paginate using these. 
      More listed here: https://developers.plex.com/docs/receipts-api/list-receipts
    """

    name = "receipts"
    path = "/purchasing/v1/receipts"
    pagination_start_key = "receiveDateBegin"
    pagination_end_key = "receiveDateEnd"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("receiveDateBegin", th.DateTimeType),
        th.Property("receiveDateEnd", th.DateTimeType),
        th.Property("receiveById", th.UUIDType),
        th.Property("poLineItemId", th.UUIDType),
        th.Property("supplierId", th.UUIDType),
        th.Property("poId", th.UUIDType),
        th.Property("partId", th.UUIDType),
        th.Property("itemId", th.UUIDType),
        th.Property("operationCode", th.StringType),
        th.Property("releaseId", th.UUIDType),
        th.Property("supplierShipperNumber", th.StringType),
        th.Property("receiveDate", th.DateTimeType),
        th.Property("receivedById", th.UUIDType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("quantity", th.NumberType),
        th.Property("priceUnit", th.StringType),
        th.Property("orderUnit", th.StringType),
        th.Property("unitPrice", th.NumberType),
        th.Property("unitPriceOriginal", th.NumberType),
        th.Property("estimatedPrice", th.BooleanType),
        th.Property("inventoryUnit", th.StringType),
        th.Property("exchangeRate", th.NumberType),
        th.Property("taxRate", th.NumberType),
        th.Property("modifiedById", th.UUIDType),
    ).to_dict()

class SalesOrdersStream(DatePaginationStream):
    """Define custom stream."""

    name = "sales_orders"
    path = "/sales/v1/orders"
    replication_key = "modifiedDate"
    pagination_start_key = "modifiedByDateBegin"
    pagination_end_key = "modifiedByDateEnd"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("customerId", th.UUIDType),
        th.Property("orderNumber", th.StringType),
        th.Property("poNumber", th.StringType),
        th.Property("poNumberRevisionDateBegin", th.DateTimeType),
        th.Property("poNumberRevisionDateEnd", th.DateTimeType),
        th.Property("status", th.StringType),
        th.Property("type", th.StringType),
        th.Property("category", th.StringType),
        th.Property("orderDateBegin", th.DateTimeType),
        th.Property("orderDateEnd", th.DateTimeType),
        th.Property("expirationDateBegin", th.DateTimeType),
        th.Property("expirationDateEnd", th.DateTimeType),
        th.Property("terms", th.StringType),
        th.Property("incoTerms", th.StringType, description="International Commercial Terms"),
        th.Property("fob", th.StringType, description="Frieght on Board Items"),
        th.Property("freightTerms", th.StringType),
        th.Property("insideSalesId", th.UUIDType),
        th.Property("outsideSalesId", th.UUIDType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdDateBegin", th.DateTimeType),
        th.Property("createdDateEnd", th.DateTimeType),
        th.Property("modifiedById", th.UUIDType),
        th.Property("modifiedDateBegin", th.DateTimeType),
        th.Property("modifiedDateEnd", th.DateTimeType),
        th.Property("poNumberRevision", th.StringType),
        th.Property("poNumberRevisionDate", th.DateTimeType),
        th.Property("orderDate", th.DateTimeType),
        th.Property("expirationDate", th.DateTimeType),
        th.Property("poNumberRevision", th.StringType),
        th.Property("poNumberRevisionDate", th.DateTimeType),
        th.Property("orderDate", th.DateTimeType),
        th.Property("expirationDate", th.DateTimeType),
        th.Property("billToAddressId", th.UUIDType),
        th.Property("shipToAddressId", th.UUIDType),
        th.Property("contactId", th.UUIDType),
        th.Property("carrierId", th.UUIDType),
        th.Property("carrier", th.StringType),
        th.Property("note", th.StringType),
        th.Property("invoiceInternalNote", th.StringType),
        th.Property("invoicePrintedNote", th.StringType),
        th.Property("buildingId", th.UUIDType),
        th.Property("commissionable", th.BooleanType),
        th.Property("region", th.StringType),
        th.Property("accountingJobNo", th.StringType),
        th.Property("createdDate", th.DateTimeType),
        th.Property("modifiedDate", th.DateTimeType),
        th.Property("multipleDocksPerShipper", th.BooleanType),
        th.Property("multiplePOPerShipper", th.BooleanType),
        th.Property("outsideSalesIds", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()

class AccountsStream(PlexStream):
    """Define custom stream."""
    """
      AccountsStream endpoint has no pagination filter support 
      https://developers.plex.com/docs/chart-of-accounts-api/list-accounts
    """
    name = "accounts"
    path = "/mdm/v1/chart-of-accounts"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("accountNumber", th.StringType),
        th.Property("name", th.StringType),
        th.Property("categoryType", th.StringType),
        th.Property("active", th.BooleanType),
    ).to_dict()