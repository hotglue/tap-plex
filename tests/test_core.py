"""Tests standard tap features using the built-in SDK tests library."""

import datetime

from singer_sdk.testing import get_tap_test_class

from tap_plex.tap import TapPlex

SAMPLE_CONFIG = {
    "start_date": datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d"),
    "api_key": "your_api_key_here",
}


# Run standard built-in tap tests from the SDK:
TestTapPlex = get_tap_test_class(
    tap_class=TapPlex,
    config=SAMPLE_CONFIG,
)
